.. index::
   pair: Thème ; Femme Vie Liberté

.. _femme_liberte_2023_10:

===============================================================================================================
|MarjanYazdani| **Femme Vie Liberté**
===============================================================================================================

- :ref:`iran_exposition:exposition_iran`


**On veut la liberté comme partout dans le monde**, aquarelle, 2023
=======================================================================

.. figure:: ../images/1600/on_veut_la_liberte_comme_partout_dans_le_monde.jpg

Tour de la liberté et statue de la liberté
==================================================

.. figure:: ../images/1600/tour_de_la_liberte_et_statue_de_la_liberte.jpg

**Reine de la vie**, aquarelle, 2022
=======================================

.. figure:: ../images/1600/reine_de_la_vie.jpg


**Ange**, aquarelle, 2022
==========================

.. figure:: ../images/1600/ange.jpg

|MarjanYazdani| **La nature en moi**, aquarelle, 2023
=========================================================

.. figure:: ../images/1600/la_nature_en_moi.jpg


|MarjanYazdani| **Mère**, gouache 2015 |MarjanYazdaniNature|
=================================================================

.. figure:: ../images/1600/mere_gouache_2015.jpg

