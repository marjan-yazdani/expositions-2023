
.. _a_refaire_2023_10:

===============
A refaire
===============

Jeune_fille__a_refaire (manque le titre, trop sombre)
===========================================================

.. figure:: ../images/1600/jeune_fille__a_refaire.jpg

Fillette_a_revoir  (manque le titre, trop sombre, trop flou)
================================================================

.. figure:: ../images/1600/fillette_a_revoir.jpg

Oiseau a_refaire  (manque le titre, trop sombre)
====================================================

.. figure:: ../images/1600/oiseau_a_refaire.jpg
