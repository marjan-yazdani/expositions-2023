.. index::
   pair: Thème ; Oppression
   ! Damné
   ! Je brûle au milieu des glaçons qui sont sur son corps
   ! Dis moi que c'est un cauchemar


.. _oppression_2023_10:

===============================================================================================================
**Oppression**
===============================================================================================================

- :ref:`iran_exposition:exposition_iran`

**La faille de l'oppression** 2023
=========================================

.. figure:: ../images/1600/la_faille_de_loppression_2023.jpg


.. _maman_kian_2023_10:

**Je brûle au milieu des glaçons qui sont sur son corps**, aquarelle, 2023
====================================================================================

- :ref:`iran_luttes:kian_pirfalak`

.. figure:: images/je_brule_au_milieu_des_glacons_aquarelle_2023.jpg

.. figure:: images/commentaire_maman_kilian.jpg

   Kian Pirfalak et sa maman


**Damné**, aquarelle,  2022
====================================================================================

.. figure:: images/damne_aquarelle_2022.jpg

.. figure:: images/damne_commentaire.jpg


**Dis moi que c'est un cauchemar**, crayon et fusain, 2023
==============================================================

.. figure:: ../images/1600/dis_moi_que_cest_un_cauchemar_2023.jpg

**Docteur Farhad Maithami**
============================

.. figure:: ../images/1600/docteur_farhad_maithami.jpg


**Fereshteh Ahmadi**
======================

.. figure:: ../images/1600/fereshteh_ahmadi.jpg


**J'enterre mes rêves**
==========================

.. figure:: ../images/1600/jenterre_mes_reves.jpg



.. _maman_kilian:

**La maman de Kian Pirfalak** (Exposition de Marjan Yazdani, 2023)
======================================================================

- :ref:`iran_luttes:kian_pirfalak`

.. figure:: ../images/1600/maman_kilian.jpg


**Torture** 2023
=================

.. figure:: ../images/1600/torture_2023.jpg


**Viol**, 2023
===============

.. figure:: ../images/1600/viol_2023.jpg

