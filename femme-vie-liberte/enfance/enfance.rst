.. index::
   pair: Thème ; Enfance
   ! Enfance

.. _enfance_2023_10:

===============================================================================================================
**Enfance**
===============================================================================================================


|MarjanYazdani| **Mère**, gouache 2015 |MarjanYazdaniNature|
=================================================================

.. figure:: ../images/1600/mere_gouache_2015.jpg


Fillette coq rayon de lumiere
====================================

.. figure:: ../images/1600/fillette_coq_rayon_de_lumiere.jpg


Enfant  assis sur une table a l'école en face jambes adulte avec cartable
===========================================================================

.. figure:: ../images/1600/enfant_assis_sur_une_table_a_l_ecole_en_face_jambes_adulte_avec_cartable.jpg


Enfant  avec petite baleine
===============================

.. figure:: ../images/1600/enfant_avec_petite_baleine.jpg

Enfant  eau arbre
===================

.. figure:: ../images/1600/enfant_eau_arbre.jpg

Enfant  gateau route
=======================

.. figure:: ../images/1600/enfant_gateau_route.jpg

Enfant  loupe aquarium
============================

.. figure:: ../images/1600/enfant_loupe_aquarium.jpg

Enfant  pleurant accroche a une jambe d adulte
==================================================

.. figure:: ../images/1600/enfant_pleurant_accroche_a_une_jambe_d_adulte.jpg

Enfant  regardant sa petite baleine
========================================

.. figure:: ../images/1600/enfant_regardant_sa_petite_baleine.jpg

Enfant  souvenirs bougies gateau
===================================

.. figure:: ../images/1600/enfant_souvenirs_bougies_gateau.jpg

Enfants riants
====================

.. figure:: ../images/1600/enfants_riants.jpg


Enfant  sur baleine avec cerf volant
========================================

.. figure:: ../images/1600/enfant_sur_baleine_avec_cerf_volant.jpg

Enfant  sur baleine
=====================

.. figure:: ../images/1600/enfant_sur_baleine.jpg


Enfant  sur la lune
======================

.. figure:: ../images/1600/enfant_sur_la_lune.jpg


Enfant  sur queue de baleine homme moustachu
=================================================

.. figure:: ../images/1600/enfant_sur_queue_de_baleine_homme_moustachu.jpg


Mains aquarium poisson
==========================

.. figure:: ../images/1600/mains_aquarium_poisson.jpg

