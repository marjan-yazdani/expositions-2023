.. index::
   pair: Exposition; Femme Vie Liberté et la Liberté en moi

.. _femme_vie_liberte_2023_10:

========================================================================================================================================
|MarjanYazdani| Exposition de peinture **Femme Vie Liberté et la Liberté en moi** de Marjan Yazdani du 6 au 26 octobre 2023
========================================================================================================================================

- :ref:`iran_exposition:exposition_iran`

#Grenoble #Exposition #Dessins #FemmeVieLiberte


Exposition peinture **Femme Vie Liberté et la nature en moi**


.. figure:: images/affiche.png
   :align: center


**Ange**, aquarelle, 2022

.. figure:: images/1600/ange.jpg


Vernissage le vendredi 6 octobre 2023 de 16h30 à 20h30.
Exposition jusqu'au 26 octobre 2023.

Conteur, musique, danse, clip.


Maison des habitant-es centre ville, 2 rue du Vieux Temple 38000 Grenoble
==============================================================================

- https://www.grenoble.fr/lieu/405/137-maison-des-habitant-es-centre-ville.htm

.. figure:: images/1600/maison_habitants_2.jpg

.. figure:: images/1600/maison_habitants.jpg

Maison des habitant-es centre ville, 2 rue du Vieux Temple 38000 Grenoble

Horaires d'ouverture au public
-----------------------------------

- Lundi, mardi, mercredi et vendredi : **8h30-12h et de 13h30-17h30**
- Jeudi : **8h30-12h**, *fermé le jeudi après-midi*


Plan openstreetmap
----------------------


.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.7316553592681885%2C45.19312207667535%2C5.735195875167848%2C45.194658792335005&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.19389/5.73343">Afficher une carte plus grande</a></small>


Panneaux de l'exposition **Femme, Vie, Liberté** (mars 2023) à l'entrée
=========================================================================

- :ref:`iran_exposition:exposition_iran`

.. figure:: images/1600/panneau_entree_2.jpg

   :ref:`iran_exposition:exposition_iran`


.. figure:: images/1600/panneaux_entree.jpg

   :ref:`iran_exposition:exposition_iran`


.. figure:: images/1600/hall_entree.jpg

   :ref:`iran_exposition:exposition_iran`


Rez-de-chaussée
-----------------

.. figure:: images/1600/entree_rez_de_chaussee.jpg


Financement grenoble
------------------------

- :ref:`iran_exposition:exposition_iran`

.. figure:: images/1600/financement_grenoble.jpg


Thème 1 **Femme, Vie, Liberté**
==================================

.. toctree::
   :maxdepth: 3

   liberte/liberte


Thème 2  Nature
==================

.. toctree::
   :maxdepth: 3

   nature/nature

Thème 3  Enfance
=====================

.. toctree::
   :maxdepth: 3

   enfance/enfance

Thème 4  Oppression
=======================

.. toctree::
   :maxdepth: 3

   oppression/oppression


Photos à refaire
==================

.. toctree::
   :maxdepth: 3

   a_refaire/a_refaire.rst

