.. index::
   pair: Thème ; Nature
   ! Nature

.. _nature_2023_10:

===============================================================================================================
|MarjanYazdaniNature| **Nature**
===============================================================================================================

La nature en moi, aquarelle, 2023
=======================================

.. figure:: ../images/1600/la_nature_en_moi.jpg

Reine de la vie, aquarelle, 2022
===================================

.. figure:: ../images/1600/reine_de_la_vie.jpg


Arbre Embrassé
====================

.. figure:: ../images/1600/arbre_embrasse.jpg

Charitable
==============

.. figure:: ../images/1600/charitable.jpg


Entouré par les mirages, en attente de la pluie
===============================================

.. figure:: ../images/1600/entoure_par_les_mirages_en_attente_de_la_pluie.jpg

Les rêves 2022
==================

.. figure:: ../images/1600/les_reves_2022.jpg



Floraison
==============

.. figure:: ../images/1600/floraison.jpg

Fleur daraignee 2022
========================

.. figure:: ../images/1600/fleur_daraignee_2022.jpg

Forêt 2022
==============

.. figure:: ../images/1600/foret_2022.jpg

Guerisseur 2022
=====================

.. figure:: ../images/1600/guerisseur_2022.jpg

Miracle 2022
===================

.. figure:: ../images/1600/miracle_2022.jpg


Memoire durable 2023
=========================

.. figure:: ../images/1600/memoire_durable_2023.jpg


Oiseau du bonheur 2022
============================

.. figure:: ../images/1600/oiseau_du_bonheur_2022.jpg


Un cadeau pour le vent 2022
=====================================

.. figure:: ../images/1600/un_cadeau_pour_le_vent_2022.jpg


Les rêves, aquarelle, 2022
===============================

.. figure:: ../images/1600/les_reves_2022.jpg

Voyageur
==========

.. figure:: ../images/1600/voyageur.jpg

