
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇨🇭
.. 🇩🇪
.. 🌍 ♀️
.. 🇮🇷


.. figure:: images/marjan_yazdani_devant_feuilles.jpg
   :align: center
   :width: 200

.. _marjan_yazdani_2023:
.. _marjan_2023:

================================================================
|MarjanYazdani|  **Expositions de Marjan Yazdani** en 2023 ♀️
================================================================

.. toctree::
   :maxdepth: 6

   femme-vie-liberte/femme-vie-liberte
   ressources/ressources
